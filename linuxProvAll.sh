#!/bin/bash

# install gnome
sudo apt-get update
sudo apt-get install -y gnome
# Install X-Server gdm3. Default was lightdm. Changed to gdm3

# Fetches the list of available updates
# Strictly upgrades the current packages  
# Installs updates (new ones) 
sudo apt-get update   
sudo apt-get upgrade 
sudo apt-get dist-upgrade  

#install Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update 
sudo apt-get install -y google-chrome-stable

# install visual studio code
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install -y code

# Install Java 8
#run updates and install required binaries
sudo apt-get install -y dos2unix
sudo apt-get install -y default-jre
sudo apt-get install -y default-jdk

#for extracting iso images
sudo apt-get install -y p7zip-full

#install gradle
sudo apt-get install -y gradle

# install atom
sudo add-apt-repository -y ppa:webupd8team/atom && sudo apt-get update && sudo apt-get install -y atom 

# postgres admin tool
sudo apt-get install -y pgadmin3

#install docker on ubuntu 17.04
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
sudo apt-get update
apt-cache search docker-ce
sudo apt-get install -y docker-ce
#run docker without sudo priveleges
sudo groupadd docker
sudo gpasswd -a $USER docker
newgrp docker

#Node.js 8
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential

# 1. Add the Spotify repository signing keys to be able to verify downloaded packages
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 0DF731E45CE24F27EEEB1450EFDC8610341D9410
# 2. Add the Spotify repository
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
# 3. Update list of available packages
sudo apt-get update
# 4. Install Spotify
sudo apt-get install -y spotify-client

#Install keepass2
sudo add-apt-repository ppa:jtaylor/keepass
sudo apt-get update
sudo apt-get install -y keepass2

#kodi
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:team-xbmc/ppa
sudo apt-get update
sudo apt-get install -y kodi

#net tools (ifconfig,etc)
sudo apt install -y net-tools
#sshd
sudo apt-get install -y openssh-server

#vim 
sudo apt-get install -y vim

#install virtualBox
sudo apt install -y virtualbox

#viber
wget -O viber.deb http://download.cdn.viber.com/cdn/desktop/Linux/viber.deb
sudo dpkg -i viber.deb

#install vlc
sudo apt-get install -y vlc

#postman
wget https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz
sudo tar -xzf postman.tar.gz -C /opt
rm postman.tar.gz
sudo ln -s /opt/Postman/Postman /usr/bin/postman

#terminator sheel 
sudo apt-get install -y terminator

#install spotify
# 1. Add the Spotify repository signing keys to be able to verify downloaded packages
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 0DF731E45CE24F27EEEB1450EFDC8610341D9410
# 2. Add the Spotify repository
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
# 3. Update list of available packages
sudo apt-get update
# 4. Install Spotify
sudo apt-get install spotify-client
        

#install saltstack
sudo apt-get install -y salt-master salt-api salt-minion salt-ssh


#GitKraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo dpkg -i gitkraken-amd64.deb
rm -rf gitkraken-amd64.deb